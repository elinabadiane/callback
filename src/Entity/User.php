<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert; //pour la validation des données

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 *  fields={"username"},
 *  message= "Le Username que vous avez indiqué est déjà utilisé" )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @ORM\Column(type="string", length=180, unique=true, nullable=true)
    * @Assert\NotBlank(message="Le login ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="Le login est trop court !!")
    * @Assert\Email()
    */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
    * @var string The hashed password
    * @ORM\Column(type="string", nullable=true)
    * @Assert\Length(min="4", max="255" ,minMessage="Le mot de passe est trop court !!")
    */
    private $password;


   /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="4", max="255" ,minMessage="Le mot de passe confirmé est trop court !!")
     */
    private $confirmepassword;


    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    private $photo;

    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    private $nom;

    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    private $prenom;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(message="Le téléphone du responsable ne doit pas être vide")
     * @Assert\Regex(
     *     pattern="/^(\+[1-9][0-9]*(\([0-9]*\)|-[0-9]*-))?[0]?[1-9][0-9\-]*$/",
     *     match=true,
     *     message="Votre numero ne doit pas contenir de lettre"
     * )
    */
        private $telephone;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statut;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="client")
     */
    private $client;



    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }
    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
     
    }

    public function getConfirmepassword(): ?string
    {
        return $this->confirmepassword;
    }

    public function setConfirmepassword(string $confirmepassword): self
    {
        $this->confirmepassword = $confirmepassword;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?int
    {
        return $this->telephone;
    }

    public function setTelephone(int $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }
    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

}