<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OperationRepository")
 */
class Operation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $numoperation;

    /**
     * @ORM\Column(type="date")
     */
    private $dateoperation;

    /**
     * @ORM\Column(type="integer")
     */
    private $montantoperation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumoperation(): ?int
    {
        return $this->numoperation;
    }

    public function setNumoperation(int $numoperation): self
    {
        $this->numoperation = $numoperation;

        return $this;
    }

    public function getDateoperation(): ?\DateTimeInterface
    {
        return $this->dateoperation;
    }

    public function setDateoperation(\DateTimeInterface $dateoperation): self
    {
        $this->dateoperation = $dateoperation;

        return $this;
    }

    public function getMontantoperation(): ?int
    {
        return $this->montantoperation;
    }

    public function setMontantoperation(int $montantoperation): self
    {
        $this->montantoperation = $montantoperation;

        return $this;
    }
}
