<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

        public function __construct(UserPasswordEncoderInterface $encoder)
        {
            $this->encoder = $encoder;
        }
    
        public function load(ObjectManager $manager)
        {
            $admin1 = new User();
            $password = $this->encoder->encodePassword($admin1, 'hakimdigital2018');
            $admin1->setpassword($password);
            $admin1->setUsername('contact@hakimdigitalsa.com');
            $admin1->setRoles(["ROLE_SUPERADMIN"]);
            $confirmepassword = $this->encoder->encodePassword($admin1, 'hakimdigital2018');
            $admin1->setConfirmepassword($confirmepassword);
            $admin1->setNom('HakimDigitalsa');
            $admin1->setPrenom('');
            $admin1->setPhoto('Hakim.jpeg');
            $admin1->setTelephone('338695001');
            $admin1->setStatut('Connecter');

            $manager->persist($admin1);
            $manager->flush();
        }
}
