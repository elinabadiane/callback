<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Client;
use App\Form\UserType;
use App\Form\ClientType;
use App\Form\ClientuserType;
use App\Service\MailerService;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 *  @Route("/api")
 */
class UserController extends FOSRestController
{
    private $connecter;
    private $deconnecter;
    private $imageAng;
    private $message;

    public function __construct(MailerService  $mailer)
    {
        $this-> connecter="Connecter";
        $this-> deconnecter="Deconnecter";
        $this-> image_directory="image_directory";
        $this-> imageAng="image_ang";
        $this->mailer = $mailer;
        $this->message = 'message';

    }
   

/**
 * @Route("/inscriptionAdmin", name="inscriptionAdmin", methods={"POST"})
 */
    public function inscriptAdmin(Request $request,EntityManagerInterface $manager, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator,  \Swift_Mailer $mailer)
    {

        $admin=new User();
        $form = $this->createForm(UserType::class, $admin);
            $data=$request->request->all();
        $form->submit($data);
        if(!$form->isSubmitted() || !$form->isValid()) {
            return $this->handleView($this->view($validator->validate($form)));
        
        }
        $roles=["ROLE_ADMIN"];
        $admin->setRoles($roles);

        if($requestFile=$request->files->all()){

            $file=$requestFile['photo'];
            $extension=$file->guessExtension();
            if($extension!='png' && $extension!='jpeg' && $extension!='jpg'){
                throw new HttpException(400,'Entrer une image valide !! ');
            }

            $fileName=md5(uniqid()).'.'.$extension;//on change le nom du fichier
            $admin->setPhoto($fileName);
            $file->move($this->getParameter($this->imageAng),$fileName);
        }

        $admin->setStatut('Connecter');
        $admin->setPassword($passwordEncoder->encodePassword($admin,
        $form->get('password')->getData()
            )
            );
        $admin->setConfirmePassword($passwordEncoder->encodePassword($admin,
        $form->get('confirmepassword')->getData()
            )
            );
        $manager->persist($admin);
        $message= 'Inscription réussie';
        $response = $this->mailer->mailer('Bienvenue dans l\'application HabyCall','',$admin->getUsername(),$message);
        $manager->flush();
        return $this->handleView($this->view("Le admin s'est bien inscrit",Response::HTTP_CREATED));
    }


/**
 * @Route("/inscriptionResponsable", name="inscriptionResponsable", methods={"POST"})
 */
    public function inscriptionResponsable(Request $request,EntityManagerInterface $manager, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator,  \Swift_Mailer $mailer)
    {

        $admin=new User();
        $form = $this->createForm(UserType::class, $admin);
        $data = json_decode($request->getContent(),true);
        if(!$data){//s il n'existe pas donc on recupere directement le tableau via la request
            $data=$request->request->all();
        }
        $form->submit($data);
        if(!$form->isSubmitted() || !$form->isValid()) {
            return $this->handleView($this->view($validator->validate($form)));
        
        }
        $roles=["ROLE_RESPONSABLE"];
        $admin->setRoles($roles);

        if($requestFile=$request->files->all()){

            $file=$requestFile['photo'];
            $extension=$file->guessExtension();
            if($extension!='png' && $extension!='jpeg' && $extension!='jpg'){
                throw new HttpException(400,'Entrer une image valide !! ');
            }

            $fileName=md5(uniqid()).'.'.$extension;//on change le nom du fichier
            $admin->setPhoto($fileName);
            $file->move($this->getParameter($this->imageAng),$fileName);
        }

        $admin->setStatut('Connecter');
        $admin->setPassword($passwordEncoder->encodePassword($admin,
        $form->get('password')->getData()
            )
            );
        $admin->setConfirmePassword($passwordEncoder->encodePassword($admin,
        $form->get('confirmepassword')->getData()
            )
            );
        $manager->persist($admin);
        $message= 'Inscription réussie';
        $response = $this->mailer->mailer('Bienvenue dans l\'application HabyCall','',$admin->getUsername(),$message);
        $manager->flush();
        return $this->handleView($this->view("Le responsable s'est bien inscrit",Response::HTTP_CREATED));
    }

     /**
     * @Route("/inscriptionManager", name="inscriptionManager", methods={"POST"})
     */
    public function inscriptionManager(Request $request,EntityManagerInterface $manager, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator,  \Swift_Mailer $mailer)
    {

        $admin=new User();
        $form = $this->createForm(UserType::class, $admin);
        $data = json_decode($request->getContent(),true);
        if(!$data){//s il n'existe pas donc on recupere directement le tableau via la request
            $data=$request->request->all();
        }
        $form->submit($data);
        if(!$form->isSubmitted() || !$form->isValid()) {
            return $this->handleView($this->view($validator->validate($form)));
        
        }
        $roles=["ROLE_MANAGER"];
        $admin->setRoles($roles);

        if($requestFile=$request->files->all()){

            $file=$requestFile['photo'];
            $extension=$file->guessExtension();
            if($extension!='png' && $extension!='jpeg' && $extension!='jpg'){
                throw new HttpException(400,'Entrer une image valide !! ');
            }

            $fileName=md5(uniqid()).'.'.$extension;//on change le nom du fichier
            $admin->setPhoto($fileName);
            $file->move($this->getParameter($this->imageAng),$fileName);
        }

        $admin->setStatut('Connecter');
        $admin->setPassword($passwordEncoder->encodePassword($admin,
        $form->get('password')->getData()
            )
            );
        $admin->setConfirmePassword($passwordEncoder->encodePassword($admin,
        $form->get('confirmepassword')->getData()
            )
            );
        $manager->persist($admin);
        $message= 'Inscription réussie';
        $response = $this->mailer->mailer('Bienvenue dans l\'application HabyCall','',$admin->getUsername(),$message);
        $manager->flush();
        return $this->handleView($this->view("Le manager s'est bien inscrit",Response::HTTP_CREATED));
    }

 
    /**
    * @Route("/admin/update/{id}", name="update_admin", methods={"POST"})
    */
    public function updateAdmin(User $user,Request $request, EntityManagerInterface $manager, ValidatorInterface $validator,UserPasswordEncoderInterface $encoder){

        if(!$user){
            throw new HttpException(404,'Cet utilisateur n\'existe pas !');
        }

        $ancienPassword=$user->getPassword();
        $ancienConfirmepassword=$user->getConfirmepassword();
        $form = $this->createForm(UserType::class, $user);
        $data=json_decode($request->getContent(),true);//si json
        if(!$data){
            $data=$request->request->all();//si non json
        }
               $ancienImage=$user->getPhoto();

        $form->submit($data);
        if(!$form->isSubmitted()){
            return $this->handleView($this->view($validator->validate($form)));
        }

        if(!$user->getPhoto()){//s il ne change pas sa photo
            $user->setPhoto($ancienImage);
        }


        if($requestFile=$request->files->all()){
            $file=$requestFile['photo'];

            if($file->guessExtension()!='png' && $file->guessExtension()!='jpeg' && $file->guessExtension()!='jpg'){
                throw new HttpException(400,'Entrer une image valide !! ');
            }

            $fileName=md5(uniqid()).'.'.$file->guessExtension();//on change le nom du fichier
            $user->setPhoto($fileName);
            $file->move($this->getParameter($this->imageAng),$fileName); //definir le image_directory dans service.yaml
            $ancienPhoto=$this->getParameter($this->imageAng)."/".$ancienImage;
            if($ancienImage){
                unlink($ancienPhoto);//supprime l'ancienne
            }

        }

        $user->setPassword($ancienPassword);
        $user->setStatut($user->getStatut());
        $user->setConfirmepassword($ancienConfirmepassword);


        $manager->persist($user); 
        $manager->flush();
     
        return $this->handleView($this->view("La modification s'est bien passer",Response::HTTP_OK));
            
    }
    
     /**
    * @Route("/responsable/update/{id}", name="update_responsable", methods={"POST"})
    */
    public function updateResponsable(User $user,Request $request, EntityManagerInterface $manager, ValidatorInterface $validator,UserPasswordEncoderInterface $encoder){

        if(!$user){
            throw new HttpException(404,'Cet utilisateur n\'existe pas !');
        }

        $ancienPassword=$user->getPassword();
        $ancienConfirmepassword=$user->getConfirmepassword();
        $form = $this->createForm(UserType::class, $user);
        $data=json_decode($request->getContent(),true);//si json
        if(!$data){
            $data=$request->request->all();//si non json
        }
               $ancienImage=$user->getPhoto();

        $form->submit($data);
        if(!$form->isSubmitted()){
            return $this->handleView($this->view($validator->validate($form)));
        }

        if(!$user->getPhoto()){//s il ne change pas sa photo
            $user->setPhoto($ancienImage);
        }


        if($requestFile=$request->files->all()){
            $file=$requestFile['photo'];

            if($file->guessExtension()!='png' && $file->guessExtension()!='jpeg' && $file->guessExtension()!='jpg'){
                throw new HttpException(400,'Entrer une image valide !! ');
            }

            $fileName=md5(uniqid()).'.'.$file->guessExtension();//on change le nom du fichier
            $user->setPhoto($fileName);
            $file->move($this->getParameter($this->imageAng),$fileName); //definir le image_directory dans service.yaml
            $ancienPhoto=$this->getParameter($this->imageAng)."/".$ancienImage;
            if($ancienImage){
                unlink($ancienPhoto);//supprime l'ancienne
            }

        }

        $user->setPassword($ancienPassword);
        $user->setStatut($user->getStatut());
        $user->setConfirmepassword($ancienConfirmepassword);


        $manager->persist($user); 
        $manager->flush();
     
        return $this->handleView($this->view("La modification s'est bien passer",Response::HTTP_OK));
            
    }

 /**
    * @Route("/manager/update/{id}", name="update_manager", methods={"POST"})
    */
    public function updateManager(User $user,Request $request, EntityManagerInterface $manager, ValidatorInterface $validator,UserPasswordEncoderInterface $encoder){

        if(!$user){
            throw new HttpException(404,'Cet utilisateur n\'existe pas !');
        }

        $ancienPassword=$user->getPassword();
        $ancienConfirmepassword=$user->getConfirmepassword();
        $form = $this->createForm(UserType::class, $user);
        $data=json_decode($request->getContent(),true);//si json
        if(!$data){
            $data=$request->request->all();//si non json
        }
               $ancienImage=$user->getPhoto();

        $form->submit($data);
        if(!$form->isSubmitted()){
            return $this->handleView($this->view($validator->validate($form)));
        }

        if(!$user->getPhoto()){//s il ne change pas sa photo
            $user->setPhoto($ancienImage);
        }


        if($requestFile=$request->files->all()){
            $file=$requestFile['photo'];

            if($file->guessExtension()!='png' && $file->guessExtension()!='jpeg' && $file->guessExtension()!='jpg'){
                throw new HttpException(400,'Entrer une image valide !! ');
            }

            $fileName=md5(uniqid()).'.'.$file->guessExtension();//on change le nom du fichier
            $user->setPhoto($fileName);
            $file->move($this->getParameter($this->imageAng),$fileName); //definir le image_directory dans service.yaml
            $ancienPhoto=$this->getParameter($this->imageAng)."/".$ancienImage;
            if($ancienImage){
                unlink($ancienPhoto);//supprime l'ancienne
            }

        }

        $user->setPassword($ancienPassword);
        $user->setStatut($user->getStatut());
        $user->setConfirmepassword($ancienConfirmepassword);


        $manager->persist($user); 
        $manager->flush();
     
        return $this->handleView($this->view("La modification s'est bien passer",Response::HTTP_OK));
            
    }

    /**
    * @Route("/security/admin/bloquer/{id}", name="bloquer_debloquer_admin", methods={"GET"})
    */ 
    public function bloquerAdmin(EntityManagerInterface $manager,User $admin=null)
    {
        if($admin->getStatut() == $this->connecter){
            $admin->setStatut($this->deconnecter);
            $texte= 'User déconnecter';
        }
        else  if($admin->getStatut() == $this->deconnecter)
        {
            $admin->setStatut($this->connecter);
            $texte='User connecter';
        }
        $manager->persist($admin);
        $manager->flush();
        return $this->handleView($this->view("Candidat déconnecté",Response::HTTP_OK));
    }

 /**
    * @Route("/security/responsable/bloquer/{id}", name="bloquer_debloquer_responsable", methods={"GET"})
    */ 
    public function bloquerResponsable(EntityManagerInterface $manager,User $admin=null)
    {
        if($admin->getStatut() == $this->connecter){
            $admin->setStatut($this->deconnecter);
            $texte= 'User déconnecter';
        }
        else  if($admin->getStatut() == $this->deconnecter)
        {
            $admin->setStatut($this->connecter);
            $texte='User connecter';
        }
        $manager->persist($admin);
        $manager->flush();
        return $this->handleView($this->view("Candidat déconnecté",Response::HTTP_OK));
    }

    /**
    * @Route("/security/manager/bloquer/{id}", name="bloquer_debloquer_manager", methods={"GET"})
    */ 
    public function bloquerManager(EntityManagerInterface $manager,User $admin=null)
    {
        if($admin->getStatut() == $this->connecter){
            $admin->setStatut($this->deconnecter);
            $texte= 'User déconnecter';
        }
        else  if($admin->getStatut() == $this->deconnecter)
        {
            $admin->setStatut($this->connecter);
            $texte='User connecter';
        }
        $manager->persist($admin);
        $manager->flush();
        return $this->handleView($this->view("Candidat déconnecté",Response::HTTP_OK));
    }

/**
 * @Route("/security/client", name="client", methods={"POST"})
 * @IsGranted({"ROLE_RESPONSABLE"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */

public function creerClient (Request $request,EntityManagerInterface $manager, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator)
{
    $client = new Client();
    $connecte=$this->getUser();
    $use = $this->getDoctrine()->getRepository(User::class)->find($connecte);
    $client->addClient($use); 
    $form=$this->createForm(ClientType::class , $client);
    $form->handleRequest($request);
    $data=$request->request->all();
     $form->submit($data);
    // dd($data);
     $entityManager = $this->getDoctrine()->getManager();
     $user = new User();
     $form=$this->createForm(ClientuserType::class , $user);
     $data = json_decode($request->getContent(),true);
     if(!$data){//s il n'existe pas donc on recupere directement le tableau via la request
         $data=$request->request->all();
     }
     $form->submit($data);
     if(!$form->isSubmitted() || !$form->isValid()) {
         return $this->handleView($this->view($validator->validate($form)));
     
     }
     $roles=["ROLE_USER"];
     $user->setRoles($roles);

     if($requestFile=$request->files->all()){

         $file=$requestFile['photo'];
         $extension=$file->guessExtension();
         if($extension!='png' && $extension!='jpeg' && $extension!='jpg'){
             throw new HttpException(400,'Entrer une image valide !! ');
         }

         $fileName=md5(uniqid()).'.'.$extension;//on change le nom du fichier
         $user->setPhoto($fileName);
         $file->move($this->getParameter($this->imageAng),$fileName);
     }

     $user->setStatut('Connecter');
     $user->setPassword($passwordEncoder->encodePassword($user,
     $form->get('password')->getData()
         )
         );
     $user->setConfirmePassword($passwordEncoder->encodePassword($user,
     $form->get('confirmepassword')->getData()
         )
         );

     $entityManager->persist($client);
     $entityManager->flush();
 return new Response('Le Client a été bien ajouté ',Response::HTTP_CREATED); 

}



    /**
    * @Route("/security/lister/admins", name="admin_user", methods={"GET"})
    * @IsGranted({"ROLE_SUPERADMIN"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
    */
public function show( UserRepository $userRepository, SerializerInterface $serializer)
    {
        $profil= $userRepository->findAll();
        $tab =[];
        for($i=0; $i<count($profil);$i++){
            if($profil[$i]->getRoles()[0]=="ROLE_ADMIN") {
                array_push($tab,$profil[$i]);
        }
        }  
        return $this->json($tab,200,[],['groups' => ['show']]);
    
    }
    


}